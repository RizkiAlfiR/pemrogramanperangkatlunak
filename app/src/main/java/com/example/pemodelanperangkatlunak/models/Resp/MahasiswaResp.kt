package com.example.pemodelanperangkatlunak.models.Resp

import com.example.pemodelanperangkatlunak.models.obj.Address
import com.example.pemodelanperangkatlunak.models.obj.Company

data class MahasiswaResp (
    val id: Int,
    val name: String,
    val username: String,
    val email: String,
    val address: Address,
    val phone: String,
    val website: String,
    val company: Company
)