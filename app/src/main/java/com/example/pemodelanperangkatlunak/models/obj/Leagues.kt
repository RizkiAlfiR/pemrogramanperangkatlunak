package com.example.pemodelanperangkatlunak.models.obj

data class Leagues (
    val idLeague: String,
    val strLeague: String,
    val strSport: String,
    val strLeagueAlternate: String
)