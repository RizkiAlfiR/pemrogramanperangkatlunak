package com.example.pemodelanperangkatlunak.models.Resp

import com.example.pemodelanperangkatlunak.models.obj.Leagues

data class LeagueResp (
    val leagues: ArrayList<Leagues>
)