package com.example.pemodelanperangkatlunak.models.Resp

import com.example.pemodelanperangkatlunak.models.obj.User

data class RegisterResp (
    val status: String,
    val message: String,
    val user: User
)