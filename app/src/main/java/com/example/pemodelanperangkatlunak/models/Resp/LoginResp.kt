package com.example.pemodelanperangkatlunak.models.Resp

import com.example.pemodelanperangkatlunak.models.obj.User

data class LoginResp (
    val status: String,
    val message: String,
    val user: User
)