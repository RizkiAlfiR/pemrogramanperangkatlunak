package com.example.pemodelanperangkatlunak.models.obj

data class User (
    val name: String,
    val address: String,
    val phone: String,
    val username: String,
    val password: String
)