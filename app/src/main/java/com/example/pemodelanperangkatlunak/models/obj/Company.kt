package com.example.pemodelanperangkatlunak.models.obj

data class Company (
    val name: String,
    val catchPhrase: String,
    val bs: String
)