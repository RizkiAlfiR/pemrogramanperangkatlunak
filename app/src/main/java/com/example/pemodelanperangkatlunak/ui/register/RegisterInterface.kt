package com.example.pemodelanperangkatlunak.ui.register

interface RegisterInterface {
    fun showLoading()
    fun hideLoading()
    fun showError(msg: String)
    fun showResult(result: Any)
}