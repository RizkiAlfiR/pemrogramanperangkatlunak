package com.example.pemodelanperangkatlunak.ui.dashboard

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.pemodelanperangkatlunak.R
import com.example.pemodelanperangkatlunak.ui.daftar_league.LeagueActivity
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.startActivity

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnDaftarLeague.setOnClickListener {
            startActivity<LeagueActivity>()
        }
    }
}
