package com.example.pemodelanperangkatlunak.ui.login

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.pemodelanperangkatlunak.R
import com.example.pemodelanperangkatlunak.presenters.LoginPresenter
import com.example.pemodelanperangkatlunak.ui.dashboard.MainActivity
import kotlinx.android.synthetic.main.activity_login.*
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast

class LoginActivity : AppCompatActivity(), LoginInterface {

    lateinit var presenter: LoginPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        presenter = LoginPresenter(this, applicationContext)

        btnLogin.setOnClickListener {
            val username = edtUsername.text.toString().trim()
            val password = edtPassword.text.toString().trim()
            presenter.checkLogin(username, password)
        }

    }

    override fun showLoading() {
        TODO("Not yet implemented")
    }

    override fun hideLoading() {
        TODO("Not yet implemented")
    }

    override fun showError(msg: String) {
        TODO("Not yet implemented")
    }

    override fun showResult(result: Any) {
        toast("Berhasil Login")
        startActivity<MainActivity>()
    }
}
