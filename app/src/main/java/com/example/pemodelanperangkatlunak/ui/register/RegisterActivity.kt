package com.example.pemodelanperangkatlunak.ui.register

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.pemodelanperangkatlunak.R
import com.example.pemodelanperangkatlunak.presenters.RegisterPresenter
import com.example.pemodelanperangkatlunak.ui.dashboard.MainActivity
import kotlinx.android.synthetic.main.activity_register.*
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast

class RegisterActivity : AppCompatActivity(), RegisterInterface {

    lateinit var presenter: RegisterPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        presenter = RegisterPresenter(this, applicationContext)

        btnRegister.setOnClickListener {
            val name = edtName.text.toString().trim()
            val address = edtAdress.text.toString().trim()
            val phone = edtTelephone.text.toString().trim()
            val username = edtUsername.text.toString().trim()
            val password = edtPassword.text.toString().trim()
            presenter.checkRegister(name, address, phone, username, password)
        }
    }

    override fun showLoading() {
        TODO("Not yet implemented")
    }

    override fun hideLoading() {
        TODO("Not yet implemented")
    }

    override fun showError(msg: String) {
        TODO("Not yet implemented")
    }

    override fun showResult(result: Any) {
        toast("Berhasil Register")
        startActivity<MainActivity>()
    }
}
