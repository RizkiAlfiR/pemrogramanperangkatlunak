package com.example.pemodelanperangkatlunak.ui.daftar_league

interface LeagueInterface {
    fun showLoading()
    fun hideLoading()
    fun showError(msg: String)
    fun showResult(result: Any)
}