package com.example.pemodelanperangkatlunak.ui.login

interface LoginInterface {
    fun showLoading()
    fun hideLoading()
    fun showError(msg: String)
    fun showResult(result: Any)
}