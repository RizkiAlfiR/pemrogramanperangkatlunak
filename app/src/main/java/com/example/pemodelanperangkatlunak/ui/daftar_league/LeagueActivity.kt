package com.example.pemodelanperangkatlunak.ui.daftar_league

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.pemodelanperangkatlunak.R
import com.example.pemodelanperangkatlunak.adapter.LeagueAdapter
import com.example.pemodelanperangkatlunak.models.obj.Leagues
import com.example.pemodelanperangkatlunak.presenters.LeaguePresenter
import kotlinx.android.synthetic.main.activity_league.*

class LeagueActivity : AppCompatActivity(), LeagueInterface {

    lateinit var presenter : LeaguePresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_league)

        presenter = LeaguePresenter(this)
        presenter.getData()
    }

    override fun showLoading() {
        loadingBar.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        loadingBar.visibility = View.GONE
    }

    override fun showError(msg: String) {
        bgError.visibility = View.VISIBLE
        recycleBottom.visibility = View.GONE

        txtErrorCode.text = "404"
        txtErrorMsg.text = msg

    }

    override fun showResult(result: Any) {
        bgError.visibility = View.GONE
        recycleBottom.visibility = View.VISIBLE

        val data = result as ArrayList<Leagues>
       val adapdter = LeagueAdapter(data)
        val layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)

        recycleBottom.adapter = adapdter
        recycleBottom.layoutManager = layoutManager
        (recycleBottom.adapter as LeagueAdapter).notifyDataSetChanged()
    }
}
